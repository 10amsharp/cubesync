<?php

class cube {


    private static $_jsession_id;

    public static function sync()
    {
        curl::init();

        curlConfig::$option[curlConfig::_LOGIN][CURLOPT_URL] = str_replace( "{JSESSIONID}", self::getJSessionId(), curlConfig::$option[curlConfig::_LOGIN][CURLOPT_URL]);

        print curl::exec(curlConfig::$option[curlConfig::_LOGIN]);

        curl::close();
    }

    public static function debug()
    {
        curl::init();


        print curlConfig::$option[curlConfig::_LOGIN][CURLOPT_URL] . "\n";
        print curlConfig::$option[curlConfig::_CLASSIC][CURLOPT_URL] . "\n";
        print curlConfig::$option[curlConfig::_TURBO][CURLOPT_URL] . "\n";

        curlConfig::$option[curlConfig::_LOGIN][CURLOPT_URL] = str_replace( "{JSESSIONID}", self::getJSessionId(), curlConfig::$option[curlConfig::_LOGIN][CURLOPT_URL]);
        curlConfig::$option[curlConfig::_CLASSIC][CURLOPT_URL] = str_replace("{JSESSIONID}", self::getJSessionId(), curlConfig::$option[curlConfig::_CLASSIC][CURLOPT_URL]);
        curlConfig::$option[curlConfig::_TURBO][CURLOPT_URL] = str_replace("{JSESSIONID}", self::getJSessionId(), curlConfig::$option[curlConfig::_TURBO][CURLOPT_URL]);

        print curlConfig::$option[curlConfig::_LOGIN][CURLOPT_URL] . "\n";
        print curlConfig::$option[curlConfig::_CLASSIC][CURLOPT_URL] . "\n";
        print curlConfig::$option[curlConfig::_TURBO][CURLOPT_URL] . "\n";

        curl::close();
    }

    private static function setJSessionId($jSessionId)
    {
        if(!isset(self::$_jsession_id))
        {
            self::$_jsession_id = $jSessionId;
        }
    }

    private static function getJSessionId()
    {
        if(isset(self::$_jsession_id))
        {
            return self::$_jsession_id;
        } else
        {
            $result = curl::exec(curlConfig::$option[curlConfig::_JSESSIONID]);
            preg_match('/sessionid\s+=\s+\'([a-zA-Z0-9]+)\'/', $result, $matches);

            if(count($matches) > 1)
            {
                self::setJSessionId($matches[1]);
            } else
            {
                die('unable to retrieve sessionid');
            }
        }

        return self::$_jsession_id;
    }
}