<?php

class email {

    private static $_bcc;
    private static $_body;
    private static $_cc;
    private static $_content_type;
    private static $_headers;
    private static $_from;
    private static $_mime_version;
    private static $_reply_to;
    private static $_subject;
    private static $_to;

    public static function send()
    {
        if (mail(
            self::getTo(),
            self::getSubject(),
            self::getBody(),
            self::getHeaders()))
        {
            printf("%s\n", "Success") ;
        }
        else
        {
            error_log("email::send() failed to send mail");
        }
    }

    public static function debug()
    {
        print self::getTo() . PHP_EOL;
        print self::getSubject() . PHP_EOL;
        print self::getBody() . PHP_EOL;
        print self::getHeaders() . PHP_EOL;
    }

    public static function setBcc($bcc)
    {
        self::$_bcc = $bcc;
    }

    public static function getBcc()
    {
        if(isset(self::$_bcc))
        {
            return self::$_bcc;
        }
    }

    public static function setBody($body)
    {
        self::$_body = $body;
    }

    public static function getBody()
    {
        if(isset(self::$_body))
        {
            return self::$_body;
        }
    }

    public static function setCc($cc)
    {
        self::$_cc = $cc;
    }

    public static function getCc()
    {
        if(isset(self::$_cc))
        {
            return self::$_cc;
        }
    }

    public static function setContentType($content_type)
    {
        self::$_content_type = $content_type;
    }

    public static function getContentType()
    {
        if(isset(self::$_content_type))
        {
            return self::$_content_type;
        }
    }

    private static function getHeaders()
    {
        if(isset(self::$_headers)) {
            return
                self::$_headers;
        } else
        {
            return
                "MIME-Version: " . self::getMimeVersion() . PHP_EOL .
                "Content-type: " .  self::getContentType() . PHP_EOL .
                "Bcc: " . implode(", ", self::getBcc()) . PHP_EOL .
                "From: " . self::getFrom() . PHP_EOL .
                "Reply-To: " . self::getReplyTo() . PHP_EOL;
        }
    }

    public static function setFrom($from)
    {
        self::$_from = $from;
    }

    public static function getFrom()
    {
        if(isset(self::$_from))
        {
            return self::$_from;
        }
    }

    public static function setMimeVersion($mime_version)
    {
        self::$_mime_version = $mime_version;
    }

    public static function getMimeVersion()
    {
        if(isset(self::$_mime_version))
        {
            return self::$_mime_version;
        }
    }

    public static function setReplyTo($reply_to)
    {
        self::$_reply_to = $reply_to;
    }

    public static function getReplyTo()
    {
        if(isset(self::$_reply_to)) {
            return self::$_reply_to;
        }
    }

    public static function setSubject($subject)
    {
        self::$_subject = $subject;
    }

    public static function getSubject()
    {
        if(isset(self::$_subject))
        {
            return self::$_subject;
        }
    }

    public static function setTo($to)
    {
        self::$_to = $to;
    }

    public static function getTo()
    {
        if(isset(self::$_to))
        {
            return self::$_to;
        }
    }
}