<?php

class curl {

    private static $ch;

    public static function init()
    {
        if (empty(self::$ch))
        {
            if (!function_exists('curl_init'))
            {
                die('curl_init not installed!');
            }

            self::$ch = curl_init();
        }
    }

    public static function exec( $option = array() )
    {
        if (!function_exists('curl_init'))
        {
            die('curl_init not installed!');
        } else if (!function_exists('curl_error'))
        {
            die('curl_error not installed!');
        }

        if (!empty(self::$ch))
        {
            $default = array(
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_USERAGENT => "Mozilla/5.0",
                CURLOPT_TIMEOUT => 10
            );

            curl_setopt_array(self::$ch, ($option + $default));

            if (!$result = curl_exec(self::$ch))
            {
                trigger_error(curl_error(self::$ch));
            }

            return $result;
        }
    }

    public static function close()
    {
        if (!empty(self::$ch))
        {
            if (!function_exists('curl_close'))
            {
                die('curl_close not installed!');
            }

            curl_close(self::$ch);
        }
    }
}